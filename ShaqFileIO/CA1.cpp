// CA1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <iterator>
#include <windows.h>

using namespace std;

//TODO ERROR HANDLING, FILECHECK, ADD COLOR , ADD CLUB VAR TO PLAYERS , DO UP THE MENU
class Players
{
private:
	string m_name;

	int m_age;

	string m_position;

	int m_num;

	string m_club;
public:
	void setName(string name);

	string getName();

	void setAge(int age);

	int getAge();

	void setPosition(string position);

	string getPosition();

	void setNum(int num);

	int getNum();

	void setClub(string club);

	string getClub();
	
};

struct namecomp
{
	bool operator() (Players& other, Players& others) const {
		return  other.getName() < others.getName();
	}
};

struct agecomp
{
	bool operator() (Players& other, Players& others) const {
		return  other.getAge() < others.getAge();
	}
};

struct poscomp
{
	bool operator() (Players& other, Players& others) const {
		return  other.getPosition() < others.getPosition();
	}
};

struct numcomp
{
	bool operator() (Players& other, Players& others) const {
		return  other.getNum() < others.getNum();
	}
};

struct clubcomp
{
	bool operator() (Players& other, Players& others) const {
		return  other.getClub() < others.getClub();
	}
};

//GLOBAL VARIABLES
vector<Players> player;
Players newPlayer;
ifstream fin("data.txt");
ofstream fout("output.txt");
bool color = false;
int i = 0;

void Players::setName(string name)

{
	m_name = name;
}

string Players::getName()
{
	return m_name;
}

void Players::setAge(int age)
{
	m_age = age;
}

int Players::getAge()
{
	return m_age;
}

void Players::setPosition(string position)
{
	m_position = position;
}

string Players::getPosition()
{
	return m_position;
}

void Players::setNum(int num)
{
	m_num = num;
}

int Players::getNum()
{
	return m_num;
}

void Players::setClub(string club)
{
	m_club = club;
}

string Players::getClub()
{
	return m_club;
}

void defaultWrite()
{
	ofstream myfile("data.txt");
	if (myfile.is_open())
	{
		myfile << "\n" << "Marco Reus" << "; " << 27 << " " << "Midfielder" << " " << 11 << " " << "Dortmund";
		myfile.close();
	}
	
}

void filecheck()
{
	if (!fin)
	{
		cout << "Failed to open file";
		cout << "\nCreating File Data.txt";
		ofstream outfile("data.txt");
		outfile.open("data.txt");
		outfile.close();
		defaultWrite();
	}
	if (!fout)
	{
		cout << "Failed to open file";
		cout << "\nCreating File Output.txt";
		ofstream outfile("output.txt");
	}
}

int checkAge()
{
	int n;
	static bool check = true;
	cin >> n;
	while (!cin)
	{
		
		if (check) {
			cout << "\nNot a valid number. Please reenter: "; //code idea from https://stackoverflow.com/questions/22573235/how-can-i-avoid-bad-input-from-a-user 
		}check = false;
		cin.clear();
		cin.ignore();
		cin >> n;
	}
	while (n >= 45 || n <= 15)
	{
		cout << "\nNot a valid player age(>44 OR <16). Please reenter: ";
		cin >> n;
	}
	return n;
}

int numcheck()
{
	int n;
	static bool check = true;
	cin >> n;
	while (!cin)
	{

		if (check) {
			cout << "\nNot a valid number. Please reenter: "; //code idea from https://stackoverflow.com/questions/22573235/how-can-i-avoid-bad-input-from-a-user 
		}check = false;
		cin.clear();
		cin.ignore();
		cin >> n;
	}
	while (n >= 100 || n <= 0)
	{
		cout << "\nNot a valid player Kit Number(>0 OR <100). Please reenter: ";
		cin >> n;
	}
	return n;
}

string namecheck()
{
	string s;
	cin.ignore();
	getline(cin, s, '\n');
	while (s.empty())
	{
		cout << "\nNot a valid name (your input is empty). \nPlease reenter: ";
		getline(cin, s, '\n');
	}
	while(s.length() > 20)
	{
		cout << "\nNot a valid name length(under 20 characters). \nPlease reenter: ";
		cin >> s;
	}
	return s;
}

string positioncheck()
{
	string s;
	cin.ignore();
	getline(cin, s, '\n');
	int z = 0;
	while (s.empty())
	{
		cout << "\nNot a valid position (your input is empty). \nPlease reenter: ";
		getline(cin, s, '\n');
	}
	while (z != 1)// 
	{
		if (s == "Forward")
		{
			z = 1;
		}
		else if (s == "Midfielder")
		{
			z = 1;
		}
		else if (s == "Defender")
		{
			z = 1;
		}
		else if (s == "Goalkeeper")
		{
			z = 1;
		}
		else 
		{
			cout << "\nNot a valid club(Goalkeeper,Defender,Midfielder,Forward). \nPlease reenter: ";
			cin >> s;
		}
		
	}
	while (s.length() > 15)
	{
		cout << "\nNot a valid position length(under 15 characters). Please reenter: ";
		cin >> s;
	}
	return s + ";";
}

string clubcheck()
{
	string s;
	cin.ignore();
	getline(cin, s, '\n');
	while (s.empty())
	{
		cout << "\nNot a valid club (your input is empty). \nPlease reenter: ";
		getline(cin, s, '\n');
	}
	s = s + ";";
	int z = 0;
	while (z != 1)// 
	{
		if (s == "Liverpool;")
		{
			z = 1;
		}
		else if (s == "Everton;")
		{
			z = 1;
		}
		else if (s == "Burnley;")
		{
			z = 1;
		}
		else if (s == "Cork;")
		{
			z = 1;
		}
		else if (s == "Dortmund;")
		{
			z = 1;
		}
		else
		{
			cout << "\nNot a valid club(Liverpool,Everton,Burnley,Cork,Dortmund). \nPlease reenter: ";
			cin >> s;
			s = s + ";";
		}
		
	}
	return s;
}

void SetColor(int ForgC) //code from https://stackoverflow.com/questions/29574849/how-to-change-text-color-and-console-color-in-codeblocks 
{
	WORD wColor;

	HANDLE hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
	CONSOLE_SCREEN_BUFFER_INFO csbi;

	//We use csbi for the wAttributes word.
	if (GetConsoleScreenBufferInfo(hStdOut, &csbi))
	{
		//Mask out all but the background attribute, and add in the forgournd color
		wColor = (csbi.wAttributes & 0xF0) + (ForgC & 0x0F);
		SetConsoleTextAttribute(hStdOut, wColor);
	}
	return;
}

void clubcolor(Players p)
{
	int z = 0;
	while (z != 1 && color == true)
	{
		if (p.getClub() == "Liverpool;")
		{
			SetColor(4);
			z = 1;
		}
		else if (p.getClub() == "Everton;")
		{
			SetColor(1);
			z = 1;
		}
		else if (p.getClub() == "Burnley;")
		{
			SetColor(6);
			z = 1;
		}
		else if (p.getClub() == "Cork;")
		{
			SetColor(2);
			z = 1;
		}
		else if (p.getClub() == "Dortmund;")
		{
			SetColor(14);
			z = 1;
		}
		
	}
}

string empty()
{
	string s;
	cin.ignore();
	getline(cin, s, '\n');
	while (s.empty())
	{
		cout << "\nNot a valid input (your input is empty). \nPlease reenter: ";
		getline(cin, s, '\n');
	}
	return s;
}

void readdata()
{
	string oneWord;
	string newName;
	string newPosition;
	string newClub;
	int newAge;
	int newnum;
	
	
	
	do
	{
		newName = "";
		do
		{
			fin >> oneWord;
			newName += oneWord;
			if (oneWord[oneWord.size() - 1] != ';')
			{
				newName += ' ';
			}
		} while (oneWord[oneWord.size() - 1] != ';');
		newName[newName.size() - 1] = ' ';
		newPlayer.setName(newName);

		fin >> newAge;
		newPlayer.setAge(newAge);

		fin >> newPosition;
		newPlayer.setPosition(newPosition);

		fin >> newnum;
		newPlayer.setNum(newnum);

		fin >> newClub;
		newPlayer.setClub(newClub);

		player.push_back(newPlayer);
		i++;
		
	} while (!fin.eof());
	fin.close();
}

void displayplayers()
{
	for (i = 0; i < player.size(); i++) //displaying the current players
	{
		clubcolor(player[i]);
		cout << "Name: " << player[i].getName() << " age: " << player[i].getAge() << " position: " << player[i].getPosition() << " Number: " << player[i].getNum() << " club: " << player[i].getClub() << endl;
		SetColor(15);
	}
}

void writetooutput()
{
	filecheck();

	ofstream fout("data.txt");
	for (i = 0; i < player.size(); i++) //writing the players to the output.txt file
	{
		fout <<"\n"<< player[i].getName() << "; " << player[i].getAge() << " " << player[i].getPosition() <<" "<< player[i].getNum()<<" "<< player[i].getClub();
	}
	cout << "Writing Finshed" << endl;
	fout.close();
}

void addplayers()
{
	vector<Players> addplayer;
	string addName;
	string addPosition;
	string addClub;
	int addAge;
	int addNum;
	char addanother;
	int add = 0;
	while (add == 0)
	{
		cout << "Enter Players Name :" << endl;
		addName = namecheck();
		newPlayer.setName(addName);
	
		cout << "Enter Players Age :" << endl;
	    addAge = checkAge();
		newPlayer.setAge(addAge);

		cout << "Enter Players Position :" << endl;
		addPosition = positioncheck();
		addPosition = addPosition + ";";
		newPlayer.setPosition(addPosition);

		cout << "Enter Players Kit Number :" << endl;
		addNum = numcheck();
		newPlayer.setNum(addNum);

		cout << "Enter Players Club :" << endl;
		addClub = clubcheck();
		newPlayer.setClub(addClub);

		player.push_back(newPlayer);
		i++;

		for (i = 0; i < player.size() ; i++) //displaying the current players
		{
			clubcolor(player[i]);
			cout << "Name: " << player[i].getName() << " age: " << player[i].getAge() << " position: " << player[i].getPosition() << " Number: " << player[i].getNum() << " club: " << player[i].getClub() << endl;
			SetColor(15);
		}

		cout << "Would you like to add another Player ? (y/n)" << endl;
		cin >> addanother;
		switch (addanother) // idea got from http://www.cplusplus.com/forum/beginner/4602/ 
		{
			case 'y':
				break;
			case 'Y':
				break;
			case 'N':
				add = 1;
				break;
			case 'n':
				add = 1;
				break;
			default: cout << "\nYour input is invalid, Please enter y/n to continue";
		}
	}
}

void deleteplayers()
{
	int selectdelete = 0;
	int d = 0;
	cout << "\nEnter the number of the entry you wish to delete:\n" << endl;
	for (i = 0; i < player.size(); i++) //displaying the current players
	{
		clubcolor(player[i]);
		cout << d<< ":"<<"Name: " << player[i].getName() << " age: " << player[i].getAge() << " position: " << player[i].getPosition() << " Number: " << player[i].getNum() << " club: " << player[i].getClub() << endl;
		SetColor(15);
		d++;
	}
	cin >> selectdelete;
	player.erase(player.begin() + selectdelete);
	cout << "\nPlayer Deleted\n" << endl;
}

void editplayers()
{
	int selectedit = 0;
	int d = 0;
	int selectvar = 0;
	string editName = "";
	int editAge;
	int editNum;
	string editClub = "";
	string editPosition = "";
	cout << "\nEnter the number of the entry you wish to edit:\n" << endl;
	for (i = 0; i < player.size(); i++) //displaying the current players
	{
		clubcolor(player[i]);
		cout <<d << ":" << "Name: " << player[i].getName() << " age: " << player[i].getAge() << " position: " << player[i].getPosition() << " Number: " << player[i].getNum() << " club: " << player[i].getClub() << endl;
		SetColor(15);
		d++;
	}
	cin >> selectedit;
	cout << "\nEnter the number of the property you wish to edit:\n" << endl;
	cout << "\n1:Name\n" << endl;
	cout << "\n2:Age\n" << endl;
	cout << "\n3:Position\n" << endl;
	cout << "\n4:KitNum\n" << endl;
	cout << "\n5:Club\n" << endl;
	cin >> selectvar;
	if (selectvar == 1)
	{
		cout <<"You are Editing \n" << "Name: " << player[selectedit].getName() << " age: " << player[selectedit].getAge() << " position: " << player[selectedit].getPosition() << " Number: " << player[selectedit].getNum() << " club: " << player[selectedit].getClub() << endl;
		cout << "\nEnter the players new name\n" << endl;
		editName = namecheck();
		player.at(selectedit).setName(editName);
	}
	else if (selectvar == 2)
	{
		cout << "You are Editing \n" << "Name: " << player[selectedit].getName() << " age: " << player[selectedit].getAge() << " position: " << player[selectedit].getPosition() << " Number: " << player[selectedit].getNum() << " club: " << player[selectedit].getClub() << endl;
		cout << "\nEnter the players new age\n" << endl;
		editAge = checkAge();
		player.at(selectedit).setAge(editAge);
	}
	else if (selectvar == 3)
	{
		cout << "You are Editing \n" << "Name: " << player[selectedit].getName() << " age: " << player[selectedit].getAge() << " position: " << player[selectedit].getPosition() << " Number: " << player[selectedit].getNum() << " club: " << player[selectedit].getClub() << endl;
		cout << "\nEnter the players new position\n" << endl;
		editPosition = positioncheck();
		player.at(selectedit).setPosition(editPosition);
	}
	else if (selectvar == 4)
	{
		cout << "You are Editing \n" << "Name: " << player[selectedit].getName() << " age: " << player[selectedit].getAge() << " position: " << player[selectedit].getPosition() << " Number: " << player[selectedit].getNum() << " club: " << player[selectedit].getClub() << endl;
		cout << "\nEnter the players new kit number\n" << endl;
		editNum = numcheck();
		player.at(selectedit).setNum(editNum);
	}
	else if (selectvar == 5)
	{
		cout << "You are Editing \n" << "Name: " << player[selectedit].getName() << " age: " << player[selectedit].getAge() << " position: " << player[selectedit].getPosition() << " Number: " << player[selectedit].getNum() << " club: " << player[selectedit].getClub() << endl;
		cout << "\nEnter the players new club\n" << endl;
		editClub = clubcheck();
		player.at(selectedit).setClub(editClub);
	}

	for (i = 0; i < player.size(); i++) //displaying the current players
	{
		clubcolor(player[i]);
		cout << "Name: " << player[i].getName() << " age: " << player[i].getAge() << " position: " << player[i].getPosition() << " Number: " << player[i].getNum() << " club: " << player[i].getClub() << endl;
		SetColor(15);
	}
}

void searchplayers()
{
	vector<Players>::iterator iter;
	string searchName = "";
	string searchClub = "";
	int selectvar = 0;
	int searchAge = 0;
	int searchNum = 0;
	int i = 0;
	string searchPosition = "";
	cout << "\nEnter the number of the property you wish to search for:\n" << endl;
	cout << "\n1:Name\n" << endl;
	cout << "\n2:Age\n" << endl;
	cout << "\n3:Position\n" << endl;
	cout << "\n4:KitNumber\n" << endl;
	cout << "\n5:Club\n" << endl;
	cin >> selectvar;
	if (selectvar == 1)
	{
		cout << "\nEnter the name you wish to search for:\n" << endl;
		searchName = namecheck();
		iter = find_if(player.begin(), player.end(),
			[&searchName](Players p) { return p.getName() == searchName + "  "; }
		
		);
		if (iter != player.end())
		{
			i = iter - player.begin();
			cout << "\nFound: ";
			clubcolor(player[i]);
			cout << "\nName: " << player[i].getName() << "        age: " << player[i].getAge() << "          position: " << player[i].getPosition() << endl;
			SetColor(15);
		}
		else 
		{
			cout << "There is no Player that matches your search Criteria" << endl;
		}
	}
	else if (selectvar == 2)
	{
		cout << "\nEnter the age you wish to search for:\n" << endl;
		searchAge = checkAge();
		iter = find_if(player.begin(), player.end(),
			[&searchAge](Players p) { return p.getAge() ==  searchAge; }
		);
		if (iter != player.end())
		{
			i = iter - player.begin();
			cout << "\nFound: ";
			clubcolor(player[i]);
			cout << "\nName: " << player[i].getName() << "        age: " << player[i].getAge() << "          position: " << player[i].getPosition() << "\nKitNumber: " << player[i].getNum() << " Club: " << player[i].getClub() << endl;
			SetColor(15);
		}
		else
		{
			cout << "There is no Player's Age that matches your search Criteria" << endl;
		}
	}
	else if (selectvar == 3)
	{
		cout << "\nEnter the position you wish to search for:\n" << endl;
	    searchPosition = empty();
		iter = find_if(player.begin(), player.end(),
			[&searchPosition](Players p) { return p.getPosition() == searchPosition+";"; }
		);
		if (iter != player.end())
		{
			i = iter - player.begin();
			cout << "\nFound: ";
			clubcolor(player[i]);
			cout << "\nName: " << player[i].getName() << "        age: " << player[i].getAge() << "          position: " << player[i].getPosition() << "\nKitNumber: " << player[i].getNum() << " Club: " << player[i].getClub() << endl;
			SetColor(15);
		}
		else
		{
			cout << "There is no Position that matches your search Criteria" << endl;
		}
	}
	else if (selectvar == 4)
	{
		cout << "\nEnter the Kit Number you wish to search for:\n" << endl;
		searchNum = numcheck();
		iter = find_if(player.begin(), player.end(),
			[&searchNum](Players p) { return p.getNum() == searchNum; }
		);
		if (iter != player.end())
		{
			i = iter - player.begin();
			cout << "\nFound: ";
			clubcolor(player[i]);
			cout << "\nName: " << player[i].getName() << "        age: " << player[i].getAge() << "          position: " << player[i].getPosition()<< "\nKitNumber: " << player[i].getNum()<< " Club: " << player[i].getClub() << endl;
			SetColor(15);
		}
		else
		{
			cout << "There is no Kit Number that matches your search Criteria" << endl;
		}
	}
	else if (selectvar == 5)
	{
		cout << "\nEnter the Club you wish to search for:\n" << endl;
		searchClub = empty();
		iter = find_if(player.begin(), player.end(),
			[&searchClub](Players p) { return p.getClub() == searchClub + ";"; }
		);
		if (iter != player.end())
		{
			i = iter - player.begin();
			cout << "\nFound: ";
			clubcolor(player[i]);
			cout << "\nName: " << player[i].getName() << "        age: " << player[i].getAge() << "          position: " << player[i].getPosition() << "\nKitNumber: " << player[i].getNum() << " Club: " << player[i].getClub() << endl;
			SetColor(15);
		}
		else
		{
			cout << "There is no Club that matches your search Criteria" << endl;
		}
	}

}

void sortbyName()
{
	
	vector<Players> sortplayer = player;
	sort(sortplayer.begin(), sortplayer.end(), namecomp() );
	for (i = 0; i < player.size(); i++) //displaying the current sorted players
	{
		clubcolor(player[i]);
		cout << "Name: " << sortplayer[i].getName() << " age: " << sortplayer[i].getAge() << " position: " << sortplayer[i].getPosition() << " Number: " << sortplayer[i].getNum() << " club: " << sortplayer[i].getClub() << endl;
		SetColor(15);
	}
	
}

void sortbyAge()
{

	vector<Players> sortplayer = player;
	sort(sortplayer.begin(), sortplayer.end(), agecomp());
	for (i = 0; i < player.size(); i++) //displaying the current sorted players
	{
		clubcolor(player[i]);
		cout << "Name: " << sortplayer[i].getName() << " age: " << sortplayer[i].getAge() << " position: " << sortplayer[i].getPosition() << " Number: " << sortplayer[i].getNum() << " club: " << sortplayer[i].getClub() << endl;
		SetColor(15);
	}

}

void sortbyPosition()
{

	vector<Players> sortplayer = player;
	sort(sortplayer.begin(), sortplayer.end(), poscomp());
	for (i = 0; i < player.size(); i++) //displaying the current sorted players
	{
		clubcolor(player[i]);
		cout << "Name: " << sortplayer[i].getName() << " age: " << sortplayer[i].getAge() << " position: " << sortplayer[i].getPosition() << " Number: " << sortplayer[i].getNum() << " club: " << sortplayer[i].getClub() << endl;
		SetColor(15);
	}

}

void sortbyKitNumber()
{

	vector<Players> sortplayer = player;
	sort(sortplayer.begin(), sortplayer.end(), numcomp());
	for (i = 0; i < player.size(); i++) //displaying the current sorted players
	{
		clubcolor(player[i]);
		cout << "Name: " << sortplayer[i].getName() << " age: " << sortplayer[i].getAge() << " position: " << sortplayer[i].getPosition() << " Number: " << sortplayer[i].getNum() << " club: " << sortplayer[i].getClub() << endl;
		SetColor(15);
	}

}

void sortbyClub()
{

	vector<Players> sortplayer = player;
	sort(sortplayer.begin(), sortplayer.end(), clubcomp());
	for (i = 0; i < player.size(); i++) //displaying the current sorted players
	{
		clubcolor(player[i]);
		cout << "Name: " << sortplayer[i].getName() << " age: " << sortplayer[i].getAge() << " position: " << sortplayer[i].getPosition() << " Number: " << sortplayer[i].getNum() << " club: " << sortplayer[i].getClub() << endl;
		SetColor(15);
	}

}
int sortmenucheck()
{
	int n = 0;
	cin >> n;
	while (n > 6 || n < 1)
	{
		cout << "\nYour input is invalid please input a number from 1 to 6" << endl;
		cin >> n;
	}
	return n;
}
void sortmenu()
{

	int choice = 0;
	int repeat = 0;
	string stars(29, '*');
	while (repeat == 0)
	{
		cout << stars;
		cout << "\n*1: Sort Players by Name    *" << endl;
		cout << "*2: Sort Players by Age     *" << endl;
		cout << "*3: Sort Players by Position*" << endl;
		cout << "*4: Sort Players by KitNum  *" << endl;
		cout << "*5: Sort Players by Club    *" << endl;
		cout << "*6: Exit		    *" << endl;
		cout << stars + "\n";

		choice = sortmenucheck();
		switch (choice)
		{
		case 1:
			sortbyName();
			break;
		case 2:
			sortbyAge();
			break;
		case 3:
			sortbyPosition();
			break;
		case 4:
			sortbyKitNumber();
			break;
		case 5:
			sortbyClub();
			break;
		case 6:
			repeat = 1;
		}
	}

}

void colouronoff()
{
	int add = 0;
	while (add != 1)
	{
		char choice;
		cout << "\nWould you like Color to display according to the club? (Y/N)" << endl;
		cin >> choice;
		switch (choice) // idea got from http://www.cplusplus.com/forum/beginner/4602/ 
		{
		case 'y':
			color = true;
			add = 1;
			break;
		case 'Y':
			color = true;
			add = 1;
			break;
		case 'N':
			add = 1;
			color = false;
			break;
		case 'n':
			add = 1;
			color = false;
			break;
		default: cout << "\nYour input is invalid, Please enter y/n to continue";
		}
	}
}
int menucheck()
{
	int n = 0;
	cin >> n;
	while (n > 8 || n < 1)
	{
		cout << "\nYour input is invalid please input a number from 1 to 8" << endl;
		cin >> n;
	}
	return n;
}
void menu()
{
	int choice = 0;
	int repeat = 0;
	string stars(29,'*');
	while (repeat == 0)
	{
		cout << stars;
		cout << "\n*1: Display Players	    *" << endl;
		cout << "*2: Add Players		    *" << endl;
		cout << "*3: Sort Players Menu       *" << endl;
		cout << "*4: Delete Players	    *" << endl;
		cout << "*5: Edit Players	    *" << endl;
		cout << "*6: Search Players	    *" << endl;
		cout << "*7: Color On/Off	    *" << endl;
		cout << "*8: Exit		    *" << endl;
		cout << stars + "\n";

		choice = menucheck();
		switch (choice)
		{
		case 1:
			displayplayers();
			break;
		case 2:
			addplayers();
			break;
		case 3:
			sortmenu();
			break;
		case 4:
			deleteplayers();
			break;
		case 5:
			editplayers();
			break;
		case 6:
			searchplayers();
			break;
		case 7:
			colouronoff();
			break;
		case 8:
			repeat = 1;
		}
	}
	
}


int main(void)
{
	filecheck();
	readdata();
	menu();
	writetooutput();
	return 0;
}